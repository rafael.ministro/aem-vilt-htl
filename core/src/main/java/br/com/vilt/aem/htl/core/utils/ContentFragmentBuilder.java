package br.com.vilt.aem.htl.core.utils;

import br.com.vilt.aem.htl.core.models.ElementPokemon;
import br.com.vilt.aem.htl.core.models.Pokemon;
import br.com.vilt.aem.htl.core.models.impl.PokemonImpl;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.cq.dam.cfm.ContentFragmentException;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.*;
import java.util.stream.Collectors;

public class ContentFragmentBuilder {


    // Identifica que esta classe sera invocada via request http
    private SlingHttpServletRequest request;


    //uitilzada pra identificar o resource correta (model) dentro do AEM
    private ResourceResolver resourceResolver;

    // Utilizando para efetivar a pesquisa e recuperar o content fragment em questao
    private QueryBuilder queryBuilder;

    public ContentFragmentBuilder(SlingHttpServletRequest request, ResourceResolver resourceResolver) {
        this.request = request;
        this.resourceResolver = resourceResolver;
        this.queryBuilder = request.getResourceResolver().adaptTo(QueryBuilder.class);
    }


    private Map<String, String> inicializarMap(String cfModel, String fragmentNumber) {
        final Map<String, String> map = new HashedMap<>();


        map.put("type", "dam:Asset");
        map.put("path", "/content/dam");
        map.put("boolproperty", "jcr:content/contentFragment");
        map.put("boolproperty.value", "true");
        map.put("1_property", "jcr:content/data/cq:model");
        map.put("2_property", "jcr:content/cq:name");
//        map.put("1_property.value","/conf/viltAemHtl/settings/dam/cfm/models/pokemon");
//        map.put("2_property.value","007");
        map.put("1_property.value", cfModel);
        map.put("2_property.value", fragmentNumber);


        return map;
    }

    private SearchResult executeQuery(Map<String, String> map) {
        Query query = queryBuilder.createQuery(
                PredicateGroup.create(map),
                request.getResourceResolver().adaptTo(Session.class)
        );

        return query.getResult();
    }

    public void buildPokemon(Pokemon pokemon,String cfModel, String fragmentNumber) throws ContentFragmentException {

        Map<String, String> map = inicializarMap(cfModel, fragmentNumber);
        SearchResult searchResult = executeQuery(map);

        for (Hit hit : searchResult.getHits()) {
            ContentFragment contentFragment = getContentFragment(hit);

            if (contentFragment != null) {
                pokemon.setPokemonName(getTextValue(contentFragment, "pokemonName"));
                pokemon.setPhotographPokemon(getTextValue(contentFragment, "photographPokemon"));
                pokemon.setNumberPokedex(getIntValue(contentFragment, "numberPokedex"));
                pokemon.setElementType(getElementList(contentFragment, getListValue(contentFragment,"elementType")));

            }

        }

    }

    private String getTextValue(ContentFragment pokemon, String attribute) {
        return Objects.requireNonNull(
                pokemon.getElement(attribute).getValue().getValue().toString()

        );
    }

    private Integer getIntValue(ContentFragment pokemon, String attribute) {
        return Integer.valueOf(
                getTextValue(pokemon, attribute)
        );
    }
    private String[] getListValue(ContentFragment pokemon, String attribute) {
        return (String[]) Objects.requireNonNull(
                pokemon.getElement(attribute).getValue().getValue()
        );
    }

    private List<ElementPokemon> getElementList(ContentFragment pokemon, String[] dlementList) {

        return Arrays.stream(dlementList).map(ElementPokemon::valueOf).collect(Collectors.toList());

    }

    private ContentFragment getContentFragment(Hit hit) {

        ContentFragment contentFragment = null;


        try {
            contentFragment = hit.getResource().adaptTo(ContentFragment.class);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }


        return contentFragment;
    }

}
