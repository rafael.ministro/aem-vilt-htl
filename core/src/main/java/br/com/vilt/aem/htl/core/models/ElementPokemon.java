package br.com.vilt.aem.htl.core.models;


/*
    Representação de um elemento de pokemon
 */
public enum ElementPokemon {

    FIRE("Fogo"),
    WATER("Água"),
    GRASS("Grama");

    private final String descricao;

    ElementPokemon(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
