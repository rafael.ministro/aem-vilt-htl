package br.com.vilt.aem.htl.core.models.impl;

import br.com.vilt.aem.htl.core.models.ElementPokemon;
import br.com.vilt.aem.htl.core.models.Pokemon;
import br.com.vilt.aem.htl.core.utils.ContentFragmentBuilder;
import com.adobe.cq.dam.cfm.ContentFragmentException;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.RequestAttribute;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import javax.annotation.PostConstruct;
import java.util.List;


@Model(
        adaptables = {SlingHttpServletRequest.class},
        adapters = {Pokemon.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = {PokemonImpl.RESOURCE_TYPE}

)
public class PokemonImpl implements Pokemon {

    protected static final String RESOURCE_TYPE = "/viltAemHtl/models/components/pokemon";

    private String pokemonName;
    private Integer numberPokedex;
    private List<ElementPokemon> elementType;
    private String photographPokemon;


    @Self
    private SlingHttpServletRequest request;

    @SlingObject
    private ResourceResolver resourceResolver;

    @RequestAttribute
    private String pokedex;


    @PostConstruct
    protected void init() throws ContentFragmentException {

        ContentFragmentBuilder contentFragmentBuilder = new ContentFragmentBuilder(request, resourceResolver);
        contentFragmentBuilder.buildPokemon(this, "/conf/viltAemHtl/settings/dam/cfm/models/pokemon", pokedex);


    }


    public String getPokemonName() {
        return pokemonName;
    }

    public void setPokemonName(String pokemonName) {
        this.pokemonName = pokemonName;
    }

    public Integer getNumberPokedex() {
        return numberPokedex;
    }

    public String getNumberPokedexFormatado() {

        if (numberPokedex == null)
            return null;

        return String.format("#%03d", numberPokedex);
    }

    public void setNumberPokedex(Integer numberPokedex) {
        this.numberPokedex = numberPokedex;
    }

    public List<ElementPokemon> getElementType() {
        return elementType;
    }

    public void setElementType(List<ElementPokemon> elementType) {
        this.elementType = elementType;
    }

    public String getPhotographPokemon() {
        return photographPokemon;
    }

    public void setPhotographPokemon(String photographPokemon) {
        this.photographPokemon = photographPokemon;
    }
}
