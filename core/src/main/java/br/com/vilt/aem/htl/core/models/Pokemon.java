package br.com.vilt.aem.htl.core.models;

import java.util.List;

public interface Pokemon {


    String getPokemonName();

    void setPokemonName(String pokemonName);

    Integer getNumberPokedex();

    void setNumberPokedex(Integer numberPokedex);

    List<ElementPokemon> getElementType();

    void setElementType(List<ElementPokemon> elementType);


    String getPhotographPokemon();

    void setPhotographPokemon(String photographPokemon);


}
